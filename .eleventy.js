const MarkdownIt = require('markdown-it');
const md = new MarkdownIt();

module.exports = function(eleventyConfig) {
    eleventyConfig.addWatchTarget('src/scss');

    eleventyConfig.addFilter('markdown', value => md.renderInline(value));

    // Copy unaltered original images and JS.
    eleventyConfig.addPassthroughCopy('src/img');
    eleventyConfig.addPassthroughCopy('src/js');

    // Prevent iframe embeds.
    eleventyConfig.addPassthroughCopy('_headers');

    return {
        dir: {
            input: 'src',
            output: 'public',
        },
    };
};
